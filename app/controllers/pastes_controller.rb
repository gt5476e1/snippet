class PastesController < ApplicationController
    def index
        @pastes = Paste.all.order("created_at DESC")
    end
    
    def new
        @paste = Paste.new
    end
    
    def create
        @paste = Paste.new(paste_params)
        
        if @paste.title == ""
            @paste.title = "Untitled"
        end
        
        if @paste.save
            redirect_to Paste.last
        else
            render 'new'
        end
        
    end
    
    def show
        @paste = Paste.find(params[:id])
    end
    
    private
    
        def paste_params
            params.require(:paste).permit(:title, :description)
        end
end
